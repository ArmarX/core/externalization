/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    externalization::ArmarXObjects::core
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <string>

#include <ArmarXCore/core/ComponentPlugin.h>

#include <ArmarXCore/core/Component.h>

namespace armarx::externalization::detail
{

    class ClientPluginBase : public ComponentPlugin
    {
    public:
        using ComponentPlugin::ComponentPlugin;

        void postCreatePropertyDefinitions(PropertyDefinitionsPtr& defs) override;
        void preOnInitComponent() override;

        template <class ManagerPrxT>
        void _preOnConnectComponent(ManagerPrxT& manager)
        {
            parent().getProxy(manager,
                              parent<armarx::Component>().getProperty<std::string>(
                                  externalizationManagerPropertyName));
            ARMARX_CHECK(manager);
        }

    public:

        static const std::string externalizationManagerPropertyName;
        static const std::string externalizationManagerPropertyDefaultValue;
        static const std::string externalizationManagerPropertyDescription;

    };


} // namespace armarx::externalization::detail
