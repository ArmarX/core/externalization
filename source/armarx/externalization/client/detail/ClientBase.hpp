#pragma once


namespace armarx::externalization
{
    template <class InterfacePrxT>
    class ClientBase
    {
    public:
        ClientBase()
        {
        }

        ClientBase(InterfacePrxT manager) : manager(manager)
        {
        }


        operator bool() const
        {
            return bool(manager);
        }

    protected:
        InterfacePrxT manager;
    };

} // namespace armarx::externalization
