#include "DataClient.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::externalization
{

    DataClient::DataClient(DataClientInterfacePrx manager) : Base(manager)
    {
    }

    CommitResult
    DataClient::commit(const Commit& commit)
    {
        return Writer::commit(commit);
    }

    FrameUpdateResult
    DataClient::commit(const FrameUpdate& update)
    {
        return Writer::commit(update);
    }

    FrameUpdateResult
    DataClient::commit(const armem::MemoryID& entityID,
                       const aron::data::DictPtr& instanceData,
                       core::time::DateTime timeCreated)
    {
        return Writer::commit(entityID, {instanceData}, timeCreated);
    }

} // namespace armarx::externalization
