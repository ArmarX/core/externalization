#pragma once

#include "detail/ClientBase.hpp"
#include "forward_declarations.h"
#include <armarx/externalization/core/Frame.h>
#include <armarx/externalization/core/ice/manager/CoordinationClientInterface.h>


namespace armarx::externalization
{
    class CoordinationClient : public ClientBase<CoordinationClientInterfacePrx>
    {
    public:
        using ClientBase::ClientBase;

        /// Push a frame onto the stack.
        dto::PushFrameOutput pushFrame(const Frame& frame);
        /// Pop a frame onto the stack.
        dto::PopFrameOutput popFrame(const Frame& frame);

        /// Enter a scope which takes care of pushing and popping the frame using RAII.
        Scope scope(const Frame& frame) const;
    };


    /**
     * @brief A RAII-style client that pushes / pops the specified frame
     * when constructed / destructed.
     */
    class Scope
    {
    public:
        /// Construct a scope, pushing a frame.
        Scope(CoordinationClient client, const Frame& frame);

        /// Destruct the scope, popping the frame.
        ~Scope();

        // Prevent copying.
        Scope(const Scope&) = delete;
        Scope& operator=(const Scope&) = delete;

        // Allow moving.
        Scope(Scope&& rhs);
        Scope& operator=(Scope&& rhs);


    public:
        CoordinationClient client;
        Frame frame;
        bool popToken = false;
    };

} // namespace armarx::externalization
