#include "BackendClient.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <armarx/externalization/core/aron_conversions.h>
#include <armarx/externalization/core/ice_conversions.h>


namespace armarx::externalization
{

    BackendQueryResult BackendClient::query(const std::vector<std::string>& modalities)
    {
        ARMARX_CHECK_NOT_NULL(manager);

        const dto::QueryFrameDataOutput response = manager->queryFrameData(dto::QueryFrameDataInput{.modalities = modalities});

        BackendQueryResult result;
        fromIce(response.frameStack, result.frameStack);

        for (const auto& [modality, dtoFrameNameToDataMap] : response.modalityToFrameNameToDataMap)
        {
            auto& frameNameToDataMap = result.modalityToFrameNameToDataMap[modality];
            for (const auto& [frameName, data] : dtoFrameNameToDataMap)
            {
                frameNameToDataMap[frameName] = aron::data::Dict::FromAronDictDTO(data);
            }
        }

        return result;
    }


} // namespace armarx::externalization
