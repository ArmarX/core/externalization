#include "CoordinationClient.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/ice_conversions.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <armarx/externalization/core/ice_conversions.h>

namespace armarx::externalization
{

    dto::PushFrameOutput
    CoordinationClient::pushFrame(const Frame& frame)
    {
        ARMARX_CHECK(manager);

        dto::PushFrameInput input{.frame = armarx::toIce<dto::Frame>(frame)};
        dto::PushFrameOutput output = manager->pushFrame(input);
        return output;
    }


    dto::PopFrameOutput
    CoordinationClient::popFrame(const Frame& frame)
    {
        ARMARX_CHECK(manager);

        dto::PopFrameInput input{.frame = armarx::toIce<dto::Frame>(frame)};
        dto::PopFrameOutput output = manager->popFrame(input);
        return output;
    }

    Scope
    CoordinationClient::scope(const Frame& frame) const
    {
        return Scope(*this, frame);
    }


    Scope::Scope(CoordinationClient client, const Frame& frame) : client(client), frame(frame)
    {
        ARMARX_CHECK_NOT_NULL(client);
        client.pushFrame(frame);
        popToken = true;
    }


    Scope::~Scope()
    {
        if (client and popToken)
        {
            client.popFrame(frame);
        }
    }

    Scope::Scope(Scope&& rhs)
    {
        *this = std::move(rhs);
    }

    Scope&
    Scope::operator=(Scope&& rhs)
    {
        this->client = rhs.client;
        rhs.client = {};

        this->frame = rhs.frame;
        rhs.frame = {};

        this->popToken = rhs.popToken;
        // Clear the pop token for RHS.
        rhs.popToken = false;

        return *this;
    }

} // namespace armarx::externalization
