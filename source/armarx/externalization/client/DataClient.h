#pragma once

#include <RobotAPI/libraries/armem/client/Writer.h>

#include <armarx/externalization/core/ice/manager/DataClientInterface.h>


namespace armarx::externalization
{
    using FrameUpdate = armem::EntityUpdate;
    using FrameUpdateResult = armem::EntityUpdateResult;
    using Commit = armem::Commit;
    using CommitResult = armem::CommitResult;


    class DataClient : public armarx::armem::client::Writer
    {
    public:
        using Base = armarx::armem::client::Writer;

    public:
        DataClient(DataClientInterfacePrx manager = nullptr);


        // Replicate `armem::Writer`'s interface to allow usage of local type names.

        CommitResult commit(const Commit& commit);

        FrameUpdateResult commit(const FrameUpdate& update);

        FrameUpdateResult commit(const armem::MemoryID& entityID,
                                 const aron::data::DictPtr& instanceData,
                                 armarx::DateTime timeCreated);
    };

} // namespace armarx::externalization
