#pragma once

#include <map>

#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>

#include <armarx/externalization/client/detail/ClientBase.hpp>
#include <armarx/externalization/core/FrameStack.h>
#include <armarx/externalization/core/ice/manager/BackendInterface.h>


namespace armarx::externalization
{
    struct BackendQueryResult
    {
        using FrameName = std::string;
        using Modality = std::string;
        using Data = aron::data::DictPtr;

        using FrameNameToDataMap = std::map<FrameName, Data>;
        using ModalityToFrameNameToDataMap = std::map<Modality, FrameNameToDataMap>;

        FrameStack frameStack;
        ModalityToFrameNameToDataMap modalityToFrameNameToDataMap;
    };


    class BackendClient : public ClientBase<BackendInterfacePrx>
    {
    public:
        using ClientBase::ClientBase;

        BackendQueryResult query(const std::vector<std::string>& modalities);
    };

} // namespace armarx::externalization
