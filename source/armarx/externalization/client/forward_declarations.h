#pragma once


namespace armarx::externalization
{

    class BackendClient;
    class BackendClientPlugin;
    struct BackendQueryResult;

    class CoordinationClient;
    class CoordinationClientPlugin;
    class Scope;

    class DataClient;
    class DataClientPlugin;

} // namespace armarx::externalization

namespace armarx
{

    using ExternalizationBackendClient = externalization::BackendClient;
    using ExternalizationBackendClientPlugin = externalization::BackendClientPlugin;
    using ExternalizationBackendQueryResult = externalization::BackendQueryResult;

    using ExternalizationCoordinationClient = externalization::CoordinationClient;
    using ExternalizationCoordinationClientPlugin = externalization::CoordinationClientPlugin;
    using ExternalizationScope = externalization::Scope;

    using ExternalizationDataClient = externalization::DataClient;
    using ExternalizationDataClientPlugin = externalization::DataClientPlugin;

} // namespace armarx
