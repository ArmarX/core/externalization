#pragma once


#include <ArmarXCore/core/ComponentPlugin.h>

#include "BackendClient.h"
#include "detail/ClientPluginBase.h"


namespace armarx::externalization
{

    class BackendClientPlugin : public detail::ClientPluginBase, public BackendClient
    {
    public:
        using detail::ClientPluginBase::ClientPluginBase;

        void preOnConnectComponent() override;
    };

} // namespace armarx::externalization
