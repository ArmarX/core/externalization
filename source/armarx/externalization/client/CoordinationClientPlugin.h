#pragma once


#include <ArmarXCore/core/ComponentPlugin.h>

#include "CoordinationClient.h"
#include "detail/ClientPluginBase.h"


namespace armarx::externalization
{

    class CoordinationClientPlugin : public detail::ClientPluginBase, public CoordinationClient
    {
    public:

        using detail::ClientPluginBase::ClientPluginBase;

        void preOnConnectComponent() override;

    };

} // namespace armarx::externalization
