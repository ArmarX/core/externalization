#include "DataClientPlugin.h"

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/application/properties/PropertyUser.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

namespace armarx::externalization
{

    void
    DataClientPlugin::preOnConnectComponent()
    {
        _preOnConnectComponent(manager);
        this->setWritingMemory(manager);
    }

} // namespace armarx::externalization
