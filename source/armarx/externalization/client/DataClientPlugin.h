#pragma once

#include <ArmarXCore/core/ComponentPlugin.h>

#include "DataClient.h"
#include "detail/ClientPluginBase.h"

namespace armarx::externalization
{

    class DataClientPlugin : public detail::ClientPluginBase, public DataClient
    {
    public:
        using detail::ClientPluginBase::ClientPluginBase;

        void preOnConnectComponent() override;

    protected:
        DataClientInterfacePrx manager;
    };

} // namespace armarx::externalization
