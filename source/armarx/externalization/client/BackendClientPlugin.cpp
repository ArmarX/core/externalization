#include "BackendClientPlugin.h"

#include <ArmarXCore/core/application/properties/PropertyUser.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::externalization
{

    void
    BackendClientPlugin::preOnConnectComponent()
    {
        _preOnConnectComponent(manager);
    }

} // namespace armarx::externalization
