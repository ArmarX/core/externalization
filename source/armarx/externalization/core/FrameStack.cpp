#include "FrameStack.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::externalization
{


    FrameStack::FrameStack()
    {
    }

    bool
    FrameStack::empty() const
    {
        return _framesBottomToTop.empty();
    }

    size_t
    FrameStack::size() const
    {
        return _framesBottomToTop.size();
    }

    const Frame&
    FrameStack::top() const
    {
        return _framesBottomToTop.back();
    }

    Frame&
    FrameStack::top()
    {
        return _framesBottomToTop.back();
    }

    void
    FrameStack::clear()
    {
        _framesBottomToTop.clear();
    }

    void
    FrameStack::pushFrame(const Frame& frame)
    {
        _framesBottomToTop.push_back(frame);
    }

    void
    FrameStack::popFrame(const Frame& frame)
    {
        // ToDo: Throw custom exceptions.
        ARMARX_CHECK_POSITIVE(_framesBottomToTop.size());
        ARMARX_CHECK_EQUAL(_framesBottomToTop.back().name, frame.name)
            << "Tried to pop a frame that does not match the top frame.";

        _framesBottomToTop.pop_back();
    }

    std::vector<Frame>
    FrameStack::toVector() const
    {
        std::vector<Frame> frames;
        frames.reserve(this->_framesBottomToTop.size());

        // Insert in reversed order.
        for (const Frame& frame : *this)
        {
            frames.push_back(frame);
        }

        return frames;
    }

    std::vector<Frame>
    FrameStack::toVectorBottomToTop() const
    {
        return _framesBottomToTop;
    }

    FrameStack
    FrameStack::FromVector(const std::vector<Frame>& framesFromTopToBottom)
    {
        FrameStack stack;
        stack._framesBottomToTop.reserve(framesFromTopToBottom.size());

        // Insert in reversed order.
        std::reverse_copy(framesFromTopToBottom.begin(),
                          framesFromTopToBottom.end(),
                          std::back_inserter(stack._framesBottomToTop));
        return stack;
    }

    FrameStack
    FrameStack::FromVectorBottomToTop(const std::vector<Frame>& framesFromBottomToTop)
    {
        FrameStack stack;
        stack._framesBottomToTop = framesFromBottomToTop;
        return stack;
    }

    FrameStack::ConstIterator
    FrameStack::begin() const
    {
        return _framesBottomToTop.rbegin();
    }

    FrameStack::ConstIterator
    FrameStack::end() const
    {
        return _framesBottomToTop.rend();
    }


    std::ostream&
    operator<<(std::ostream& os, const FrameStack& fs)
    {
        os << "FrameStack with " << fs.size() << " frames:";
        size_t i = 0;
        for (const Frame& frame : fs)
        {
            os << "\n- [" << i++ << "] '" << frame.name << "'";
        }
        return os;
    }

} // namespace armarx::externalization
