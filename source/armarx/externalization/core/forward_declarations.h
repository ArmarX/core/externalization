#pragma once


namespace armarx::externalization
{

    struct Frame;
    class FrameStack;

} // namespace armarx::externalization

namespace armarx::externalization::arondto
{

    class Frame;
    class FrameStack;

} // namespace armarx::externalization::arondto

namespace armarx::externalization::dto
{

    struct Frame;
    struct FrameStack;

} // namespace armarx::externalization::dto

namespace armarx
{

    using ExternalizationFrame = externalization::Frame;
    using ExternalizationFrameStack = externalization::FrameStack;

} // namespace armarx
