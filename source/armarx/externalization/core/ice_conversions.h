#pragma once

#include "forward_declarations.h"


namespace armarx::externalization
{

    // ICE DTO <-> BO

    void fromIce(const dto::Frame& dto, Frame& bo);
    void toIce(dto::Frame& dto, const Frame& bo);

    void fromIce(const dto::FrameStack& dto, FrameStack& bo);
    void toIce(dto::FrameStack& dto, const FrameStack& bo);
}


namespace armarx::externalization::arondto
{

    // ICE DTO <-> ARON

    void fromIce(const dto::Frame& dto, Frame& aron);
    void toIce(dto::Frame& dto, const Frame& aron);

    void fromIce(const dto::FrameStack& dto, FrameStack& aron);
    void toIce(dto::FrameStack& dto, const FrameStack& aron);

} // namespace armarx::externalization
