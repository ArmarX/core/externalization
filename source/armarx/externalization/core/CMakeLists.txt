armarx_add_ice_library(core_ice
    SLICE_FILES
        ice/datatypes.ice
        ice/manager/BackendInterface.ice
        ice/manager/CoordinationClientInterface.ice
        ice/manager/DataClientInterface.ice
        ice/manager/ManagerInterface.ice
    DEPENDENCIES
        ArmarXCoreInterfaces
        RobotAPIInterfaces
)


armarx_add_aron_library(core_aron
    ARON_FILES
        aron/FrameStack.xml
)


armarx_add_library(core
    SOURCES
        aron_conversions.cpp
        ice_conversions.cpp

        Frame.cpp
        FrameStack.cpp

    HEADERS
        core.h
        aron_conversions.h
        forward_declarations.h
        ice_conversions.h

        Frame.h
        FrameStack.h

    DEPENDENCIES_PUBLIC
        # ArmarXCore
        ArmarXCoreInterfaces
        ArmarXCore
        # RobotAPI
        aron
        armem

        armarx_externalization::core_aron
        armarx_externalization::core_ice

    # DEPENDENCIES_PRIVATE
        # ...
    # DEPENDENCIES_INTERFACE
        # ...
    # DEPENDENCIES_LEGACY_PUBLIC
        # ...
    # DEPENDENCIES_LEGACY_PRIVATE
        # ...
    # DEPENDENCIES_LEGACY_INTERFACE
        # ...
)


# Disable until needed.
if (FALSE)

    armarx_add_test(coreTest
        TEST_FILES
            test/coreTest.cpp
        DEPENDENCIES
            externalization::core
    )

endif()
