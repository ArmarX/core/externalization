#include "aron_conversions.h"

#include <ArmarXCore/core/ice_conversions.h>

#include "Frame.h"
#include "FrameStack.h"
#include <armarx/externalization/core/aron/FrameStack.aron.generated.h>
#include <armarx/externalization/core/ice/datatypes.h>


namespace armarx
{
    void
    externalization::fromAron(const arondto::Frame& dto, Frame& bo)
    {
        armarx::fromIce(dto.name, bo.name);
    }

    void
    externalization::toAron(arondto::Frame& dto, const Frame& bo)
    {
        armarx::toIce(dto.name, bo.name);
    }

    void
    externalization::fromAron(const arondto::FrameStack& dto, FrameStack& bo)
    {
        bo.clear();
        for (const arondto::Frame& dtoFrame : dto.framesBottomToTop)
        {
            Frame frame;
            fromAron(dtoFrame, frame);
            bo.pushFrame(frame);
        }
    }

    void
    externalization::toAron(arondto::FrameStack& dto, const FrameStack& bo)
    {
        dto.framesBottomToTop.clear();
        for (const Frame& frame : bo.toVectorBottomToTop())
        {
            toAron(dto.framesBottomToTop.emplace_back(), frame);
        }
    }

} // namespace armarx
