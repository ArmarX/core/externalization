#pragma once

#include <vector>

#include "Frame.h"


namespace armarx::externalization
{

    /**
     * @brief
     */
    class FrameStack
    {
    public:
        using Container = std::vector<Frame>;
        using ConstIterator = Container::const_reverse_iterator;

    public:
        FrameStack();

        bool empty() const;
        size_t size() const;

        const Frame& top() const;
        Frame& top();

        /**
         * @brief Iterate through the stack from top to bottom.
         */
        ConstIterator begin() const;
        ConstIterator end() const;

        /**
         * @return Get the current frame stack with the top-most frame first.
         */
        std::vector<Frame> toVector() const;
        std::vector<Frame> toVectorBottomToTop() const;

        static FrameStack FromVector(const std::vector<Frame>& framesFromTopToBottom);
        static FrameStack FromVectorBottomToTop(const std::vector<Frame>& framesFromBottomToTop);


        void clear();

        /**
         * @brief Push a frame onto the stack.
         * @param frame The frame.
         */
        void pushFrame(const Frame& frame);
        /**
         * @brief Pop a frame from the stack.
         * @param frame The frame.
         */
        void popFrame(const Frame& frame);



        friend std::ostream& operator<<(std::ostream& os, const FrameStack& fs);


    public:
        // A std::stack does not let us see past the top element.
        // Therefore, we use a std::vector with push_back() and pop_back()
        // as a replacement.

        /// The frames in the stack, from bottom to top.
        Container _framesBottomToTop;
    };

} // namespace armarx::externalization
