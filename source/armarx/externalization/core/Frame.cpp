#include "Frame.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::externalization
{

    std::ostream&
    operator<<(std::ostream& os, const Frame& f)
    {
        return os << "<Frame name='" << f.name << "'>";
    }

} // namespace armarx::externalization
