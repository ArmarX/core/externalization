/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * package    externalization::core
 * author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * date       2022
 * copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *            GNU General Public License
 */


#pragma once

#include <armarx/externalization/core/ice/datatypes.ice>

#include <Ice/BuiltinSequences.ice>

#include <RobotAPI/interface/aron.ice>


module armarx
{
    module externalization
    {
        module dto
        {
            dictionary<string, armarx::aron::data::dto::Dict> FrameNameToDataMap;
            dictionary<string, FrameNameToDataMap> ModalityToFrameNameToDataMap;
            struct QueryFrameDataInput
            {
                Ice::StringSeq modalities;
            };
            struct QueryFrameDataOutput
            {
                ModalityToFrameNameToDataMap modalityToFrameNameToDataMap;
                FrameStack frameStack;
            };
        };

        interface BackendInterface
        {
            dto::QueryFrameDataOutput queryFrameData(dto::QueryFrameDataInput input);
        };
    };
};
