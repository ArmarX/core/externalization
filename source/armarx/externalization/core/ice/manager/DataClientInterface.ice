/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * package    externalization::core
 * author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * date       2022
 * copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *            GNU General Public License
 */


#pragma once

#include <RobotAPI/interface/armem/commit.ice>
#include <RobotAPI/interface/armem/server/WritingMemoryInterface.ice>

#include <armarx/externalization/core/ice/datatypes.ice>


module armarx
{
    module externalization
    {
        // DataClient

        module dto
        {
            // We do not reinvent the wheel here. But this also might bind us too strongly.
            // Poor man's alias.
            struct CommitFrameDataInput
            {
                armarx::armem::data::Commit commit;
            };
            struct CommitFrameDataOutput
            {
                armarx::armem::data::CommitResult result;
            };
        };

        interface DataClientInterface extends armarx::armem::server::WritingMemoryInterface
        {
        };
    };
};
