/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * package    externalization::core
 * author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * date       2022
 * copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *            GNU General Public License
 */


#pragma once

#include <armarx/externalization/core/ice/manager/BackendInterface.ice>
#include <armarx/externalization/core/ice/manager/CoordinationClientInterface.ice>
#include <armarx/externalization/core/ice/manager/DataClientInterface.ice>



module armarx
{
    module externalization
    {
        interface ManagerInterface extends
                BackendInterface,
                CoordinationClientInterface,
                DataClientInterface
        {
        };
    };
};
