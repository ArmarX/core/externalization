#pragma once

#include <string>


namespace armarx::externalization
{

    /**
     * @brief An element of the frame stack.
     */
    struct Frame
    {
        std::string name;

        friend std::ostream& operator<<(std::ostream& os, const Frame& f);
    };

} // namespace armarx::externalization
