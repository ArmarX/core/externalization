#include "ice_conversions.h"

#include <ArmarXCore/core/ice_conversions.h>

#include "Frame.h"
#include "aron_conversions.h"
#include <armarx/externalization/core/aron/FrameStack.aron.generated.h>
#include <armarx/externalization/core/ice/datatypes.h>


namespace armarx
{

    template <class AronT, class DtoT, class BoT>
    void
    fromIceViaAron(const DtoT& dto, BoT& bo)
    {
        auto aron = AronT::FromAron(dto.data);
        fromAron(aron, bo);
    }
    template <class AronT, class DtoT, class BoT>
    void
    toIceViaAron(DtoT& dto, const BoT& bo)
    {
        AronT aron;
        toAron(aron, bo);
        toIce(dto, aron);
    }


    // ICE DTO <-> BO

    void
    externalization::fromIce(const dto::Frame& dto, Frame& bo)
    {
        fromIceViaAron<arondto::Frame>(dto, bo);
    }
    void
    externalization::toIce(dto::Frame& dto, const Frame& bo)
    {
        toIceViaAron<arondto::Frame>(dto, bo);
    }


    void
    externalization::fromIce(const dto::FrameStack& dto, FrameStack& bo)
    {
        fromIceViaAron<arondto::FrameStack>(dto, bo);
    }
    void
    externalization::toIce(dto::FrameStack& dto, const FrameStack& bo)
    {
        return toIceViaAron<arondto::FrameStack>(dto, bo);
    }
}


namespace armarx::externalization
{

    // ICE DTO <-> ARON

    void
    arondto::fromIce(const dto::Frame& dto, Frame& aron)
    {
        aron.fromAron(dto.data);
    }
    void
    arondto::toIce(dto::Frame& dto, const Frame& aron)
    {
        dto.data = aron.toAronDTO();
    }


    void
    arondto::fromIce(const dto::FrameStack& dto, FrameStack& aron)
    {
        aron.fromAron(dto.data);
    }
    void
    arondto::toIce(dto::FrameStack& dto, const FrameStack& aron)
    {
        dto.data = aron.toAronDTO();
    }

} // namespace armarx
