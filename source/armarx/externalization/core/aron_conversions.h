#pragma once

#include "forward_declarations.h"


namespace armarx::externalization
{

    void fromAron(const arondto::Frame& dto, Frame& bo);
    void toAron(arondto::Frame& dto, const Frame& bo);

    void fromAron(const arondto::FrameStack& dto, FrameStack& bo);
    void toAron(arondto::FrameStack& dto, const FrameStack& bo);

}
