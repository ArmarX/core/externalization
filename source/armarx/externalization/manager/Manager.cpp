#include "Manager.h"

#include <ArmarXCore/core/ice_conversions.h>
#include <ArmarXCore/core/time/Clock.h>

#include <RobotAPI/libraries/armem/client/query.h>
#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>

#include <armarx/externalization/core/aron/FrameStack.aron.generated.h>
#include <armarx/externalization/core/ice_conversions.h>


namespace armarx::externalization::manager
{

    Manager::Manager()
    {
    }


    Manager::Manager(armem::server::MemoryToIceAdapter* iceAdapter,
                     const std::string& providerName) :
        iceAdapter(iceAdapter), providerName(providerName)
    {
        ARMARX_CHECK_NOT_NULL(iceAdapter);
    }


    void
    Manager::initialize()
    {
        if (not this->frameStackSegment)
        {
            ARMARX_CHECK_NOT_NULL(iceAdapter);
            armem::server::wm::Memory* workingMemory = iceAdapter->workingMemory;

            this->frameStackSegment =
                &workingMemory->addCoreSegment("FrameStack", arondto::FrameStack::ToAronType());

            this->entityID = frameStackSegment->id()
                                 .withProviderSegmentName(providerName)
                                 .withEntityName("frameStack");

            // Add an initial empty frame.
            {
                arondto::FrameStack initial;

                armem::EntityUpdate update{
                    .entityID = entityID,
                    .instancesData = {initial.toAron()},
                    .timeCreated = armarx::Clock::Now(),
                };
                frameStackSegment->update(update);
            }
        }
    }


    dto::QueryFrameDataOutput
    Manager::queryFrameData(const dto::QueryFrameDataInput& input)
    {
        ARMARX_CHECK_NOT_NULL(frameStackSegment);

        dto::QueryFrameDataOutput output;

        const arondto::FrameStack stack = getCurrentFrameStackLocking();
        output.frameStack.data = stack.toAronDTO();

        // Query the memory.
        {
            armem::client::QueryBuilder qb;
            for (const std::string& modality : input.modalities)
            {
                namespace qf = armem::client::query_fns;
                qb.coreSegments(qf::withName(modality))
                    .providerSegments(qf::all())
                    .entities(qf::all())
                    .snapshots(qf::latest());
            }

            armem::client::QueryResult result = iceAdapter->query(qb.buildQueryInput());

            for (const std::string& modality : input.modalities)
            {
                // Make sure that an entry exists for each requested modality.
                dto::FrameNameToDataMap& frameNameToDataMap =
                    output.modalityToFrameNameToDataMap[modality];

                if (result.success)
                {
                    if (armem::wm::CoreSegment* segment = result.memory.findCoreSegment(modality))
                    {
                        segment->forEachInstance(
                            [&frameNameToDataMap](const armem::wm::EntityInstance& instance)
                            {
                                if (instance.data())
                                {
                                    frameNameToDataMap[instance.id().entityName] =
                                        instance.data()->toAronDictDTO();
                                }
                            });
                    }
                }
            }
        }

        return output;
    }


    dto::PushFrameOutput
    Manager::pushFrame(const dto::PushFrameInput& input)
    {
        ARMARX_CHECK_NOT_NULL(frameStackSegment);
        DateTime now = Clock::Now();

        arondto::FrameStack stack = getCurrentFrameStackLocking();

        arondto::Frame& newFrame = stack.framesBottomToTop.emplace_back();
        fromIce(input.frame, newFrame);

        commitNewFrameStackLocking(stack, now);

        return dto::PushFrameOutput{};
    }


    dto::PopFrameOutput
    Manager::popFrame(const dto::PopFrameInput& input)
    {
        ARMARX_CHECK_NOT_NULL(frameStackSegment);
        DateTime now = Clock::Now();

        arondto::FrameStack stack = getCurrentFrameStackLocking();
        ARMARX_CHECK_POSITIVE(stack.framesBottomToTop.size())
            << "Tried to pop from an empty stack.";

        const arondto::Frame frame = armarx::fromIce<arondto::Frame>(input.frame);
        ARMARX_CHECK_EQUAL(stack.framesBottomToTop.back().name, frame.name)
            << "Tried to pop a frame that does not match the top frame.";

        stack.framesBottomToTop.pop_back();

        commitNewFrameStackLocking(stack, now);

        return dto::PopFrameOutput{};
    }


    dto::CommitFrameDataOutput
    Manager::commitFrameData(const dto::CommitFrameDataInput& input)
    {
        // Just commit.
        armem::data::CommitResult result = iceAdapter->commit(input.commit);
        return dto::CommitFrameDataOutput{
            .result = result
        };
    }


    arondto::FrameStack
    Manager::getCurrentFrameStackLocking() const
    {
        arondto::FrameStack current = frameStackSegment->doLocked(
            [this]()
            {
                const auto& entity = frameStackSegment->getEntity(entityID);
                return entity.getLatestSnapshot().getInstance(0).dataAs<arondto::FrameStack>();
            });
        return current;
    }


    void
    Manager::commitNewFrameStackLocking(const arondto::FrameStack& frameStack,
                                        const armem::Time& time) const
    {
        armem::Commit commit;
        armem::EntityUpdate& update = commit.add();
        update.entityID = entityID;
        update.timeCreated = time;
        update.instancesData = {frameStack.toAron()};

        iceAdapter->commitLocking(commit);
    }

} // namespace armarx::externalization::manager
