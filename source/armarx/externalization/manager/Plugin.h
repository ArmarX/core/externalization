#pragma once

#include <ArmarXCore/core/ComponentPlugin.h>

#include <RobotAPI/libraries/armem/server/plugins/Plugin.h>

#include "Manager.h"
#include <armarx/externalization/core/forward_declarations.h>
#include <armarx/externalization/core/ice/manager/ManagerInterface.h>


namespace armarx::externalization::manager
{

    /**
     * @brief Subscribes the memory updates topic.
     *
     * When using this plugin, the component needs to implement the
     * `MemoryListenerInterface`.
     *
     * @see MemoryListenerInterface
     */
    class Plugin : public armarx::ComponentPlugin
    {
    public:
        Plugin(ManagedIceObject& parent, std::string pre);
        virtual ~Plugin() override;

        virtual void
        postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties) override;

        virtual void preOnInitComponent() override;

    public:
        armem::server::plugins::Plugin* memoryServerPlugin = nullptr;

        std::optional<Manager> manager;
    };

} // namespace armarx::externalization::manager
