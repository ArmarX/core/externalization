#pragma once

#include <string>

#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/server/MemoryToIceAdapter.h>

#include <armarx/externalization/core/forward_declarations.h>
#include <armarx/externalization/core/ice/manager/ManagerInterface.h>


namespace armarx::externalization::manager
{

    class Manager
    {
    public:
        Manager();
        Manager(armem::server::MemoryToIceAdapter* iceAdapter,
                const std::string& providerName);

        void initialize();

    public:
        dto::QueryFrameDataOutput queryFrameData(const dto::QueryFrameDataInput& input);

        dto::PushFrameOutput pushFrame(const dto::PushFrameInput& input);
        dto::PopFrameOutput popFrame(const dto::PopFrameInput& input);

        dto::CommitFrameDataOutput commitFrameData(const dto::CommitFrameDataInput& input);


    private:
        arondto::FrameStack getCurrentFrameStackLocking() const;
        void commitNewFrameStackLocking(const arondto::FrameStack&, const armem::Time& time) const;

    public:
        armem::server::MemoryToIceAdapter* iceAdapter = nullptr;
        std::string providerName = "";

        armem::server::wm::CoreSegment* frameStackSegment = nullptr;
        armem::MemoryID entityID;
    };

} // namespace armarx::externalization::manager
