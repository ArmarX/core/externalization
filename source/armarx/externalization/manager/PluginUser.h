#pragma once

#include <ArmarXCore/core/ManagedIceObject.h>

#include <armarx/externalization/core/ice/manager/ManagerInterface.h>


namespace armarx::externalization::manager
{
    class Plugin;


    class PluginUser : virtual public armarx::ManagedIceObject, virtual public ManagerInterface
    {
    protected:
        PluginUser();
        virtual ~PluginUser() override;

    public:
        // BackendInterface
        dto::QueryFrameDataOutput queryFrameData(const dto::QueryFrameDataInput& input,
                                                 const Ice::Current& current) override;

        // CoordinationClientInterface
        dto::PushFrameOutput pushFrame(const dto::PushFrameInput& input,
                                       const Ice::Current& current) override;
        dto::PopFrameOutput popFrame(const dto::PopFrameInput& input,
                                     const Ice::Current& current) override;

    private:
        Plugin* plugin = nullptr;

    };

} // namespace armarx::externalization::manager
