#include "PluginUser.h"

#include "Plugin.h"

namespace armarx::externalization::manager
{

    PluginUser::PluginUser()
    {
        addPlugin(plugin);
    }


    PluginUser::~PluginUser()
    {
    }


    dto::QueryFrameDataOutput
    PluginUser::queryFrameData(const dto::QueryFrameDataInput& input, const Ice::Current& current)
    {
        ARMARX_CHECK_NOT_NULL(plugin);
        ARMARX_CHECK(plugin->manager.has_value());
        return plugin->manager->queryFrameData(input);
    }


    dto::PushFrameOutput
    PluginUser::pushFrame(const dto::PushFrameInput& input, const Ice::Current& current)
    {
        ARMARX_CHECK_NOT_NULL(plugin);
        ARMARX_CHECK(plugin->manager.has_value());
        return plugin->manager->pushFrame(input);
    }


    dto::PopFrameOutput
    PluginUser::popFrame(const dto::PopFrameInput& input, const Ice::Current& current)
    {
        ARMARX_CHECK_NOT_NULL(plugin);
        ARMARX_CHECK(plugin->manager.has_value());
        return plugin->manager->popFrame(input);
    }

} // namespace armarx::externalization::manager
