#include "Plugin.h"

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/ice_conversions.h>
#include <ArmarXCore/core/time/Clock.h>

#include <RobotAPI/libraries/armem/client/query.h>
#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>

#include <armarx/externalization/core/aron/FrameStack.aron.generated.h>
#include <armarx/externalization/core/ice_conversions.h>


namespace armarx::externalization::manager
{

    Plugin::Plugin(ManagedIceObject& parent, std::string pre) : armarx::ComponentPlugin(parent, pre)
    {
        addPlugin(memoryServerPlugin);
        addPluginDependency(memoryServerPlugin);
    }


    Plugin::~Plugin()
    {
    }


    void
    Plugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
    {
    }


    void
    Plugin::preOnInitComponent()
    {
        ARMARX_CHECK_NOT_NULL(memoryServerPlugin);
        this->manager = Manager(&memoryServerPlugin->iceAdapter, parent().getName());
        this->manager->initialize();
    }

} // namespace armarx::externalization::manager
