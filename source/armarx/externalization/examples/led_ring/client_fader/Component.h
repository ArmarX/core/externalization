/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    externalization::ArmarXObjects::led_ring::client_fader
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

#include <armarx/externalization/client/forward_declarations.h>
#include <armarx/externalization/examples/led_ring/client_fader/ComponentInterface.h>


namespace armarx::externalization::components::led_ring::client_fader
{

    class Component :
        virtual public armarx::Component,
        virtual public armarx::externalization::components::led_ring::client_fader::
            ComponentInterface
    {
    public:
        Component();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        /// Get the component's default name.
        static std::string GetDefaultName();


    protected:
        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;


    private:
        void run();


    private:
        static const std::string defaultName;


        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            std::string frameName = "fader";
            float updateFrequencyHz = 25;

            float faderDurationSec = 3;
            float cycleDurationSec = 4;
        };
        Properties properties;

        /// Access to other components.
        struct Remote
        {
            armarx::ExternalizationDataClientPlugin* client = nullptr;
        };
        Remote remote;

        armarx::SimpleRunningTask<>::pointer_type task;
    };

} // namespace armarx::externalization::components::led_ring::client_fader
