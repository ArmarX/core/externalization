/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    externalization::ArmarXObjects::led_ring::client_fader
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"

#include <SimoxUtility/color/Color.h>

#include <ArmarXCore/core/time/Clock.h>
#include <ArmarXCore/core/time/Metronome.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include <RobotAPI/libraries/SimpleTrajectory/Trajectory.h>
#include <RobotAPI/libraries/armem/client/plugins/Plugin.h>
#include <RobotAPI/libraries/aron/common/aron_conversions/simox.h>

#include <armarx/externalization/client/DataClientPlugin.h>
#include <armarx/externalization/examples/led_ring/core/aron/LedRing.aron.generated.h>

namespace armarx::externalization::components::led_ring::client_fader
{

    const std::string Component::defaultName = "led_ring_client_fader";


    Component::Component()
    {
        addPlugin(remote.client);
    }


    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        def->optional(properties.frameName, "p.frameName", "Name of the externalization frame.");
        def->optional(
            properties.updateFrequencyHz, "p.updateFrequencyHz", "The update frequency [Hz].");

        def->optional(properties.faderDurationSec,
                      "p.faderDurationSec",
                      "Duration of the fader animation [seconds].");
        def->optional(properties.cycleDurationSec,
                      "p.cycleDurationSec",
                      "Duration of the full animation cycle [seconds].");

        return def;
    }


    void
    Component::onInitComponent()
    {
    }


    void
    Component::onConnectComponent()
    {
        this->task = new armarx::SimpleRunningTask<>([this]() { this->run(); });
        this->task->start();
    }


    void
    Component::onDisconnectComponent()
    {
        this->task->stop();
        this->task = nullptr;
    }


    void
    Component::onExitComponent()
    {
    }

    void
    Component::run()
    {
        ARMARX_CHECK(remote.client);

        const armem::MemoryID entityID(
            "Externalization", "LedRing", getName(), properties.frameName);

        Metronome metronome(Frequency::Hertz(properties.updateFrequencyHz));
        DateTime start = Clock::Now();

        Duration faderDuration = Duration::Seconds(properties.faderDurationSec);
        Duration cycleDuration = Duration::Seconds(properties.cycleDurationSec);

        const simox::Color colorHi = simox::Color::cyan();
        const simox::Color colorLo = simox::Color::azure(128);

        trajectory::Trajectory traj;
        trajectory::VariantTrack& track = traj.addTrack("color");
        track[0.0] = colorLo.to_vector3f();
        track[0.5] = colorHi.to_vector3f();
        track[1.0] = colorLo.to_vector3f();

        while (task and not task->isStopped())
        {
            DateTime now = Clock::Now();

            // Determine parameters.
            // In [0, cycleDuration]
            float phaseSec =
                std::fmod((now - start).toSecondsDouble(), cycleDuration.toSecondsDouble());

            // In [0, cycleDuration / faderDuration], with 1.0 = faderDuration.
            float phase = phaseSec / faderDuration.toSecondsDouble();

            Eigen::MatrixXf colorVec = std::get<Eigen::MatrixXf>(traj["color"].at(phase));
            simox::Color color(colorVec.col(0).head<3>().eval());

            // Build ring.
            arondto::Key ledLo, ledHi;
            ledLo.azimuth = 0;
            ledHi.azimuth = 2 * M_PI;

            simox::toAron(ledLo.color, color);
            simox::toAron(ledHi.color, color);

            arondto::LedRing ring;
            ring.keys = {ledLo, ledHi};

            // Commit it.
            externalization::FrameUpdateResult result =
                remote.client->commit(entityID, {ring.toAron()}, now);
            if (result.success)
            {
                ARMARX_VERBOSE << "Committed frame '" << properties.frameName << "'.";
            }
            else
            {
                ARMARX_INFO << "Commit failed: " << result.errorMessage;
            }

            metronome.waitForNextTick();
        }
    }

    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }


    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }


    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

} // namespace armarx::externalization::components::led_ring::client_fader
