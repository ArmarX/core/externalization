/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    externalization::ArmarXObjects::led_ring::client_chaser
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"

#include <SimoxUtility/color/Color.h>

#include <ArmarXCore/core/time/Clock.h>
#include <ArmarXCore/core/time/Metronome.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include <RobotAPI/libraries/armem/client/plugins/Plugin.h>
#include <RobotAPI/libraries/aron/common/aron_conversions/simox.h>

#include <armarx/externalization/client/DataClientPlugin.h>
#include <armarx/externalization/examples/led_ring/core/aron/LedRing.aron.generated.h>


namespace armarx::externalization::components::led_ring::client_chaser
{

    const std::string Component::defaultName = "led_ring_client_chaser";


    Component::Component()
    {
        addPlugin(remote.client);
    }


    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        def->optional(properties.frameName, "p.frameName", "Name of the externalization frame.");
        def->optional(
            properties.updateFrequencyHz, "p.updateFrequencyHz", "The update frequency [Hz].");
        def->optional(
            properties.roundTripTimeSec, "p.roundTripTimeSec", "The round trip time [seconds].");
        def->optional(properties.chaserLengthRad,
                      "p.chaserLength",
                      "The length of the chaser portion [rad].");

        return def;
    }


    void
    Component::onInitComponent()
    {
    }


    void
    Component::onConnectComponent()
    {
        this->task = new armarx::SimpleRunningTask<>([this]() { this->run(); });
        this->task->start();
    }


    void
    Component::onDisconnectComponent()
    {
        this->task->stop();
        this->task = nullptr;
    }


    void
    Component::onExitComponent()
    {
    }

    void
    Component::run()
    {
        ARMARX_CHECK(remote.client);

        const armem::MemoryID entityID(
            "Externalization", "LedRing", getName(), properties.frameName);

        Metronome metronome(Frequency::Hertz(properties.updateFrequencyHz));
        DateTime start = Clock::Now();

        Duration roundTripTime = Duration::Seconds(properties.roundTripTimeSec);
        float chaserLength = properties.chaserLengthRad;

        while (task and not task->isStopped())
        {
            DateTime now = Clock::Now();

            // Determine parameters.

            // in [0, 1]
            float chaserPos =
                (std::fmod((now - start).toSecondsDouble(), roundTripTime.toSecondsDouble()) /
                 roundTripTime.toSecondsDouble());
            // in [0, 2 pi]
            chaserPos *= 2 * M_PI;
            float lo = chaserPos - chaserLength / 2;
            float hi = chaserPos + chaserLength / 2;

            simox::Color colorChaser = simox::Color::kit_maygreen();
            simox::Color colorBackground = simox::Color::azure();

            float eps = 1e-2;

            // Build ring.
            arondto::Key ledChaserLo, ledChaserHi, ledBackLo, ledBackHi;
            ledBackHi.azimuth = lo - eps;
            ledChaserLo.azimuth = lo;
            ledChaserHi.azimuth = hi;
            ledBackLo.azimuth = hi + eps;

            simox::toAron(ledBackLo.color, colorBackground);
            ledBackHi.color = ledBackLo.color;

            simox::toAron(ledChaserLo.color, colorChaser);
            ledChaserHi.color = ledChaserLo.color;

            arondto::LedRing ring;
            ring.keys = {ledBackHi, ledChaserLo, ledChaserHi, ledBackLo};

            // Commit it.
            externalization::FrameUpdateResult result =
                remote.client->commit(entityID, {ring.toAron()}, now);
            if (result.success)
            {
                ARMARX_VERBOSE << "Committed frame '" << properties.frameName << "'.";
            }
            else
            {
                ARMARX_INFO << "Commit failed: " << result.errorMessage;
            }

            metronome.waitForNextTick();
        }
    }


    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }


    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }


    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

} // namespace armarx::externalization::components::led_ring::client_chaser
