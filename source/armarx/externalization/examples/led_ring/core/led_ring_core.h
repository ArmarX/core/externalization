/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    externalization::ArmarXObjects::led_ring::core
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


namespace armarx
{
    /**
    * @defgroup Library-led_ring_core led_ring_core
    * @ingroup externalization
    * A description of the library led_ring_core.
    *
    * @class led_ring_core
    * @ingroup Library-led_ring_core
    * @brief Brief description of class led_ring_core.
    *
    * Detailed description of class led_ring_core.
    */
    class led_ring_core
    {
    public:

    };

}
