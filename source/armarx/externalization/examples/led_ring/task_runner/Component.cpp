/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    externalization::ArmarXObjects::led_ring::task_runner
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"

#include <ArmarXCore/core/time/Clock.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include <RobotAPI/libraries/armem/client/plugins/Plugin.h>

#include <armarx/externalization/client/CoordinationClientPlugin.h>


namespace armarx::externalization::components::led_ring::task_runner
{

    const std::string Component::defaultName = "led_ring_task_runner";

    Component::Component()
    {
        addPlugin(remote.client);
    }

    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        def->optional(properties.speed, "p.speed", "Speed factor.");

        return def;
    }


    void
    Component::onInitComponent()
    {
    }


    void
    Component::onConnectComponent()
    {
        this->task = new armarx::SimpleRunningTask<>([this]() { this->run(); });
        this->task->start();
    }


    void
    Component::onDisconnectComponent()
    {
    }


    void
    Component::onExitComponent()
    {
    }


    // This scope is only used for logging in this example.
    struct LogScope
    {
        int& depth;
        std::string name;

        LogScope(int& depth, const std::string& name) : depth(depth), name(name)
        {
            ARMARX_IMPORTANT << prefix() << "Start '" << name << "'.";
            depth++;
        }
        ~LogScope()
        {
            depth--;
            ARMARX_IMPORTANT << prefix() << "End '" << name << "'.";
        }

        std::string
        prefix() const
        {
            std::string indent = "|\t";
            std::stringstream ss;
            for (int i = 0; i < depth; ++i)
            {
                ss << indent;
            }
            return ss.str();
        }
    };


    void
    Component::run()
    {
        ARMARX_CHECK(remote.client);

        const float speed = properties.speed;

        int depth = 0;

        auto gesture = [&]()
        {
            LogScope logScope(depth, "gesture");

            Clock::WaitFor(Duration::SecondsDouble(1));
        };

        auto speakWithGesture = [&]()
        {
            LogScope logScope(depth, "speaking with gesture");

            // Enable fader in this scope.
            ExternalizationScope extScope = remote.client->scope({"fader"});

            Clock::WaitFor(Duration::SecondsDouble(1) / speed);
            gesture();
            Clock::WaitFor(Duration::SecondsDouble(1) / speed);
        };

        auto speak = [&]()
        {
            LogScope logScope(depth, "speaking");

            // Enable fader in this scope.
            ExternalizationScope extScope = remote.client->scope({"fader"});

            Clock::WaitFor(Duration::SecondsDouble(2) / speed);
        };

        auto plan = [&]()
        {
            LogScope logScope(depth, "planning");

            Clock::WaitFor(Duration::SecondsDouble(2) / speed);
        };

        auto execute = [&]()
        {
            LogScope logScope(depth, "execution");

            Clock::WaitFor(Duration::SecondsDouble(2) / speed);

            speakWithGesture();

            Clock::WaitFor(Duration::SecondsDouble(1) / speed);
        };

        auto task = [&]()
        {
            LogScope logScope(depth, "task");

            // Enable chaser in this scope.
            ExternalizationScope extScope = remote.client->scope({"chaser"});
            // Scope can be moved and frame will only be popped once.
            ExternalizationScope newExtScope = std::move(extScope);

            Clock::WaitFor(Duration::SecondsDouble(1) / speed);

            plan();

            Clock::WaitFor(Duration::SecondsDouble(1) / speed);

            execute();

            Clock::WaitFor(Duration::SecondsDouble(1) / speed);
        };

        auto main = [&]()
        {
            LogScope logScope(depth, "main");

            Clock::WaitFor(Duration::SecondsDouble(1) / speed);

            speak();

            Clock::WaitFor(Duration::SecondsDouble(1) / speed);

            task();

            Clock::WaitFor(Duration::SecondsDouble(1) / speed);
        };

        main();
    }

    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }


    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }


    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

} // namespace armarx::externalization::components::led_ring::task_runner
