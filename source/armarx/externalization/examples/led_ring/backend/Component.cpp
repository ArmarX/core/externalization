/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    externalization::ArmarXObjects::led_ring_backend
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"

#include <SimoxUtility/math/periodic.h>
#include <SimoxUtility/math/rescale.h>

#include <ArmarXCore/core/time/Clock.h>
#include <ArmarXCore/core/time/Metronome.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include <RobotAPI/libraries/aron/common/aron_conversions/simox.h>

#include <armarx/externalization/client/BackendClientPlugin.h>
#include <armarx/externalization/core/aron/FrameStack.aron.generated.h>

namespace armarx::externalization::components::led_ring::backend
{

    const std::string Component::defaultName = "led_ring_backend";


    Component::Component()
    {
        addPlugin(remote.client);
    }


    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        def->optional(properties.pollFrequencyHz,
                      "p.pollFrequencyHz",
                      "Frequency to poll the memory at [Hz].");

        def->optional(properties.numLeds, "p.numLeds", "Number of LEDs in the ring.");
        def->optional(properties.ledSize, "p.ledSize", "Size of a single LED [mm].");
        def->optional(properties.ringSize, "p.ringSize", "Diameter of the LED ring [mm].");

        return def;
    }


    void
    Component::onInitComponent()
    {
    }


    void
    Component::onConnectComponent()
    {
        this->task = new SimpleRunningTask<>([this]() { this->run(); });
        this->task->start();
    }


    void
    Component::onDisconnectComponent()
    {
        this->task->stop();
        this->task = nullptr;
    }


    void
    Component::onExitComponent()
    {
    }


    void
    Component::run()
    {
        ARMARX_CHECK(remote.client);

        Metronome metronome(armarx::Frequency::Hertz(properties.pollFrequencyHz));
        while (task and not task->isStopped())
        {
            const externalization::BackendQueryResult result = remote.client->query({"LedRing"});
            processResult(result);

            metronome.waitForNextTick();
        }
    }


    void
    Component::processResult(const BackendQueryResult& result)
    {
        viz::StagedCommit stage = arviz.stage();

        {
            viz::Layer layer = arviz.layer("Origin");
            layer.add(viz::Pose("Origin").scale(0.75));
            stage.add(layer);
        }

        auto it = result.modalityToFrameNameToDataMap.find("LedRing");
        if (it == result.modalityToFrameNameToDataMap.end())
        {
            // Nothing to do.
            return;
        }
        const BackendQueryResult::FrameNameToDataMap& frames = it->second;
        const FrameStack& frameStack = result.frameStack;

        {
            viz::Layer layer = arviz.layer("LED Ring");

            // Find matching LED ring frame.
            std::optional<arondto::LedRing> ring;
            for (const Frame& frame : frameStack)
            {
                if (auto it = frames.find(frame.name); it != frames.end())
                {
                    ARMARX_VERBOSE << "Drawing frame '" << frame.name << "'";
                    ring = arondto::LedRing::FromAron(it->second);
                    break;
                }
            }

            if (ring.has_value())
            {
                drawOn(layer, ring.value());
            }

            stage.add(layer);
        }

        int count = 0;

        // Demo mode: Draw the different frames on different layers.
        {
            for (const auto& [frameName, data] : frames)
            {
                viz::Layer layer = arviz.layer("LED Ring '" + frameName + "'");

                auto ring = externalization::arondto::LedRing::FromAron(data);
                drawOn(layer, ring);

                stage.add(layer);

                count++;
            }
        }

        ARMARX_VERBOSE << "Drawing " << count << " LED ring frames.";

        arviz.commit(stage);
    }


    void
    Component::drawOn(viz::Layer& layer, arondto::LedRing& ring)
    {
        using arondto::Key;

        auto flattenKeys = [](std::vector<Key>& keys)
        {
            // Wrap all azimuths.
            for (Key& key : keys)
            {
                key.azimuth = simox::math::periodic_clamp<float>(key.azimuth, 0, 2 * M_PI);
            }

            // Sort by azimuth.
            std::sort(keys.begin(),
                      keys.end(),
                      [](const Key& lhs, const Key& rhs) { return lhs.azimuth < rhs.azimuth; });

            //                  0                                  2 pi
            // --- (k'_n-1) --- | --- (k_0) --- ... --- (k_n-1) --- | --- k'_0
            std::vector<Key> flattenedKeys;
            flattenedKeys.reserve(keys.size() + 2);
            // Mirrored final key.
            flattenedKeys.push_back(keys.back());
            flattenedKeys.back().azimuth -= 2 * M_PI;

            // The keys in [0, 2 pi]
            std::copy(keys.begin(), keys.end(), std::back_inserter(flattenedKeys));

            // Mirrored first key.
            flattenedKeys.push_back(keys.front());
            flattenedKeys.back().azimuth += 2 * M_PI;

            return flattenedKeys;
        };

        const std::vector<Key> keys = flattenKeys(ring.keys);


        std::vector<simox::Color> ledColors(properties.numLeds, simox::Color::black());

        auto ledIndexToAzimuth = [&ledColors](int index)
        {
            float azimuth = simox::math::rescale<float>(index, 0, ledColors.size(), 0, 2 * M_PI);
            return azimuth;
        };


        // The two LEDs spanning the current interval.
        size_t keyLoIndex = 0;

        const Key* keyLo = &keys[keyLoIndex];
        const Key* keyHi = &keys[(keyLoIndex + 1) % keys.size()];

        for (size_t ledIndex = 0; ledIndex < ledColors.size(); ++ledIndex)
        {
            const float ledAzimuth = ledIndexToAzimuth(ledIndex);

            // Is this LED in the next interval?
            while (ledAzimuth >= keyHi->azimuth)
            {
                // Switch to next interval.
                keyLoIndex = keyLoIndex + 1;
                ARMARX_CHECK_LESS(keyLoIndex, keys.size());
                ARMARX_CHECK_LESS(keyLoIndex + 1, keys.size());

                keyLo = &keys[keyLoIndex];
                keyHi = &keys[keyLoIndex + 1];
            }

            const float azimLo = keyLo->azimuth;
            const float azimHi = keyHi->azimuth;

            ARMARX_CHECK_LESS_EQUAL(azimLo, azimHi);
            ARMARX_CHECK_LESS_EQUAL(azimLo, ledAzimuth);
            ARMARX_CHECK_LESS(ledAzimuth, azimHi);

            // Determine the LED's color.
            simox::Color colorLo, colorHi;
            simox::fromAron(keyLo->color, colorLo);
            simox::fromAron(keyHi->color, colorHi);

            float t = (ledAzimuth - azimLo) / (azimHi - azimLo);
            Eigen::Vector3f interpol = (1 - t) * colorLo.to_vector3f() + t * colorHi.to_vector3f();

            ledColors.at(ledIndex) = simox::Color(interpol);
        }

        // Draw the ArViz layer.
        for (size_t i = 0; i < ledColors.size(); ++i)
        {
            Eigen::Vector3f pos = Eigen::Vector3f::Zero();
            float azimuth = ledIndexToAzimuth(i);
            pos.x() = std::cos(azimuth);
            pos.y() = std::sin(azimuth);
            pos *= properties.ringSize / 2;

            layer.add(viz::Sphere("LED #" + std::to_string(i))
                          .position(pos)
                          .color(ledColors[i])
                          .radius(properties.ledSize / 2));
        }
    }


    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }


    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }


    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

} // namespace armarx::externalization::components::led_ring::backend
