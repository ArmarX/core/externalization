/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    externalization::ArmarXObjects::led_ring_backend
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <optional>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <armarx/externalization/client/forward_declarations.h>
#include <armarx/externalization/examples/led_ring/backend/ComponentInterface.h>
#include <armarx/externalization/examples/led_ring/core/aron/LedRing.aron.generated.h>

namespace armarx::externalization::components::led_ring::backend
{

    class Component :
        virtual public armarx::Component,
        virtual public armarx::externalization::components::led_ring::backend::ComponentInterface,
        virtual public armarx::ArVizComponentPluginUser
    {
    public:
        Component();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        /// Get the component's default name.
        static std::string GetDefaultName();


    protected:
        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;


    private:
        void run();
        void processResult(const BackendQueryResult& result);
        void drawOn(viz::Layer& layer, arondto::LedRing& ring);


    private:
        static const std::string defaultName;


        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            float pollFrequencyHz = 25;

            int numLeds = 60;
            float ledSize = 5.0;
            float ringSize = 100.0;
        };
        Properties properties;

        struct Remote
        {
            armarx::ExternalizationBackendClientPlugin* client = nullptr;
        };
        Remote remote;


        armarx::SimpleRunningTask<>::pointer_type task;
    };

} // namespace armarx::externalization::components::led_ring::backend
