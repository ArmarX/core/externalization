# ArmarX Externalization

The project `armarx_externalization` provides a framework and predefined data types
for externalizing internal robot state and data to users.

The concepts of this package are based on these observations:

- There are multiple **aspects** of the system that can be illustrated at any point in time.
  (Examples: perception, navigation, manipulation, speech, ...) 
- There are multiple ways or channels (aka **modalities**) that can be used to illustrate these aspects.
  (Examples: 1D time-series plots, 2D graph(ical) displays, 3D visualization, LED stripes, gestures, ...)  
- In general, per modality there is **only one (final) channel** that is visible to the user. 
  (Examples: Only one screen showing 3D visualization, only one LED stripe at the bottom of the robot.)
- Different aspects of the system are **relevant at different points in time**.
  (Examples: Movement direction during navigation, sound direction during dialog.)

This framework attempts to tackle the arising challenges by supporting developers at ...

- defining how to illustrate different aspects of the robotic systems using different modalities,
- implementing the actual display of different modalities,
- switching between the different aspects over the course of the execution of a demo or high-level skills. 


## Terms and Concepts

The main component types and data flow supported by this package can be illustrated like this: 

```
     |               |                 ^
   Robot/          High-               |
   System          Level         Actual Display
   State          Context         in Software 
     |               |            or Hardware
     v               v                 |
+---------+   +--------------+   +----------+
|  Data   |   | Coordination |   | Backends |
| Clients |   |   Clients    |   |          |
+---------+   +--------------+   +----------+
     |               |                 ^
  Display        Switching             |
   Data        Signals (Frames)    Frame Data  
     |               |             and Stack
     v               v                 |
  +----------------------------------------+
  |         Externalization Manager        |
  +----------------------------------------+
```

### Data Client (the Producer)

- Data clients **produce** display data in specific modalities based on aspects of robot's state.
- In other words, a data client specifies **what** to display to externalize an aspect of the state.
- Example: A client continuously specifies LED strip color settings 
  according to the current robot's navigation. 


### Backend (the Consumer)

- A backend **consumes** a externalization data for a specific modality and implements the actual display.
- In other words, a backend specifies **how** to display the data produced by data clients.
- Example: A backend controls an LED strip on the bottom of the robot's platform.


### Coordination Client (the Orchestrator)

- A coordination client switches the current **context** and thereby controls when different aspects
  of the system are displayed.
- In other words, a coordinator specifies **when** to display different aspects.
- Example: A high-level navigation skill switches to the `navigation` display while it is running.


### Manager (the Mediator and Data Storage)

- The main server component provided by this package.
- Serves as a data storage and mediator between clients and backends.
- It implements the [`armarx::externalization::ManagerInterface`](source/armarx/externalization/core/ice/ManagerInterface.ice).
- Internally, it is built on the ArmarX Memory framework, i.e. it is a proper memory server, but has its own interface.


### Frames and the Frame Stack

- As a running example, assume a mobile robot with a platform equipped with an LED ring.
- **Data clients** produce their data in the context of a **frame**.
  For example: 
  - Data client `A` uses the LED ring to point to the current navigation target.
    (Frame: `navigation`)
  - Data client `B` lets the LED ring point towards the current movement direction.
    (Frame: `platform movement`)
  - Data client `C` points towards close obstacles that the robot needs to dodge.
    (Frame: `obstacle avoidance`)
  - (Note: These three clients may just as well be implemented in a single component if that is sensible.) 
- **Coordination clients** orchestrate the display based on these frames using the **frame stack**.
  For example:
  1. The main task `NavigateToLocation` starts by pushing the `navigation` frame onto the stack,
    and then proceeds to plan a global path to the target location.
  2. While the path is being planned, `navigation` is the top entry of the stack and is thus displayed to the user.
  3. Once the path is computed, the `MoveAlongPath` skill is triggered to move the robot along the computed path
    (while staying in the general scope of the `NavigateToLocation` task). 
  4. The skill `MoveAlongPath` pushes the `platform movement` frame and starts moving the platform.
    Now, the direct movement direction is illustrated by the LED ring. 
  5. When the robot gets close to a dynamic obstacle, the `MoveAlongPath` skill 
    calls the `LocalReactivePlanner` skill, which pushes the `obstacle avoidance` frame 
    so the LED ring highlights close obstacles. 
  6. Once the obstacles have been dodged, the `LocalReactivePlanner` skill 
    pops the `obstacle avoidance` frame from the stack and returns to its caller.
    Now, the robot's movement direction on the global path is visible again. 
  7. When reaching the goal, the `MoveAlongPath` skills ends by popping the `platform movement` 
    frame from the stack, returning to the scope of its caller. 
  8. The `NavigateToLocation` task concludes by popping the `navigation` frame from the stack.
    The stack is now empty, and the LED ring enters an idle state.
- Note: The "frame stack" is analogous to the stack of function calls in an imperative program.
  Each scope (function call / skill) may push a new frame while it is running, and pops the frame when it is done.
  The above procedure could also be part of a larger program. 
  In that case, the LED ring shows navigation-related aspects while `NavigateToLocation` is running,
  and returns to the previous state when it is done.
- At each point in time, the LED ring **backend** ...
  1. fetches the current frame stack and the available LED ring frames,
  2. find the available frame that is highest in the frame stack,
  3. and updates the actual displays with the specified information.


## Structure of this Package

On the code level, this package contains:

- The [core library](source/armarx/externalization/core) with common data types and their conversions.
- The [client library](source/armarx/externalization/client) providing the client API classes and component plugins.
- The [manager library](source/armarx/externalization/manager) containing the implementation of the externalization_manager.
- The [component `externalization_manager`](source/armarx/externalization/components/externalization_manager).



## Example

The [LED ring example](source/armarx/externalization/examples/led_ring)
illustrates how the different roles of this system can be implemented.
It includes:
- A [backend](source/armarx/externalization/examples/led_ring/backend) visualizing a fictional LED ring via ArViz.
- Two data clients producing two frames for the LED ring.
  - [chaser](source/armarx/externalization/examples/led_ring/client_chaser): 
    A spotlight going around the LED ring.
  - [fader](source/armarx/externalization/examples/led_ring/client_fader):
    A soft fading animation of the whole LED ring.
- A [high-level task runner](source/armarx/externalization/examples/led_ring/task_runner)
  emulating a task execution which switches between these frames.
- A [core library](source/armarx/externalization/examples/led_ring/core) for the common data types.
